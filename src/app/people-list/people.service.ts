import { IHttpService, IPromise, IQService } from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { Person } from './person.model';

@Injectable('peopleService')
export class PeopleService {
  private people: Person[] = [];
  /*@ngInject*/
  constructor(private $http: IHttpService) { }

  /**
   * Get people from webservice
   */
  getPeople(): IPromise<Person[]> {
    return this.$http.get<Person[]>("people").then(response => {
      return response.data as Person[]
    }, error => {
      console.log(error);
      return [];
    });
  };
}
}
