import { Component, OnInit } from 'angular-ts-decorators';
import { Person } from './person.model';
import { PeopleService } from './people.service';

@Component({
    selector: 'app-people-list',
    transclude: true,
    template: require('./people-list.component.html'),
    styles: [require('./people-list.component.scss')]
})
export class PeopleListComponent implements OnInit {
    people: Person[] = [];
    /*@ngInject*/
    constructor(private readonly peopleService: PeopleService) {
    }

    ngOnInit() {
        this.peopleService.getPeople().then(peopleList => {
            this.people = peopleList;
        });
    }
}