import { Component, Input } from 'angular-ts-decorators';
import { Person } from '../people-list/person.model';

@Component({
  selector: 'person-detail',
  transclude: true,
  template: require('./person-detail.component.html'),
  styles: [require('./person-detail.component.scss')]
})
export class PersonDetailComponent {
  @Input() person: Person;
  isAdditionalPersonInfosOpen = false;


  public toogleAdditionalPersonInfos(){
    this.isAdditionalPersonInfosOpen = !this.isAdditionalPersonInfosOpen;
  } 
}
