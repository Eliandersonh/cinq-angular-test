import { Component, Input } from 'angular-ts-decorators';


@Component({
  selector: 'app-page',
  transclude: true,
  template: require('./page.component.html'),
  styles: [ require('./page.component.scss') ]
})
export class PageComponent {
  @Input('@') title: string;
}
