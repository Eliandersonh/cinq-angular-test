import { NgModule } from 'angular-ts-decorators';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PeopleService } from './people-list/people.service';
import './styles.css';
import { PeopleListComponent } from './people-list/people-list.component';
import { PageComponent } from './page/page.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import "@uirouter/angularjs";

@NgModule({
  id: 'AppModule',
  imports: [
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    PeopleListComponent,
    PageComponent,
    PersonDetailComponent,
  ],
  providers: [
    PeopleService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
