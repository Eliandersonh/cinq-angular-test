const express = require('express');
const app = express();
const cors = require('cors');
const webpack = require('webpack');
const middleware = require('webpack-dev-middleware');
const { buildFakePersons } = require("./people.helper");


// Used for hot reload  
const compiler = webpack({ ...require("../../webpack.config")({}, { mode: "development" }), mode:"development" });
app.use(
    middleware(compiler, {})
);
//------



app.use(cors());

app.get('', function (req, res) {
    res.send('It works!');
});

app.get('/people', function (req, res) {
    res.send(buildFakePersons());
});

app.listen(3000, function () {
    console.log('Sample people backend listening on port 3000!');
});