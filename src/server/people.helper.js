module.exports.buildFakePersons = () => {
    const names = ["Adam", "Abe", "Maria", "Rose", "Mario", "Luigi"];
    const surnames = ["Lincoln", "Franklin", "Jackson", "Miyazaki", "M'bebe"];

    const loremIpusum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit," +
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini" +
        "m veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo c" +
        "onsequat."

    const getRandomFrom = array => array[Math.ceil(Math.random() * (array.length - 1))];

    const getFullName = () => `${getRandomFrom(names)} ${getRandomFrom(surnames)}`

    return new Array(10).fill(1).map((value, index) => {
        return {
            id: index, 
            name: getFullName(), 
            disclosableInfo: loremIpusum
        };
    })
}