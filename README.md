# Cinq-angularjs-test


Project uses [angular-ts-decorators](https://github.com/vsternbach/angular-ts-decorators) to mimic angular 2+ style development in angularjs environment with typescript and webpack.

## Getting Started

To get you started you can simply clone the this repository.



### Install Dependencies

We have two kinds of dependencies in this project: development tools and application specific packages. They are both managed with npm in package.json as devDependencies and dependencies respectively.

```
npm install
```

### Running the App during Development

This test comes preconfigured with a local development webserver that run a simple api with people endpoint. It also has a webpack-dev-server, that supports hot reload.  You can start this webserver with `npm start`.

Now browse to the app at `http://localhost:3000/`.

## Tests

This projects is cofigured with units test, but there no test yet. I need to study little bit more about unit tests in angularjs because I haven't used it in a while.

### Building and running the App in Production

To build the application for production just run `npm build`, it creates dist directory that have the production optimized build.
